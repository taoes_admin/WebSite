package com.mafour.service.ding;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class BtnLink {

  private String title;

  private String actionURL;

}
