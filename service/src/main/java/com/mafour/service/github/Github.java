package com.mafour.service.github;

import lombok.Data;

@Data
public class Github {

  private String title;

  private String coverImgUrl;

  private String linkUrl;
}
