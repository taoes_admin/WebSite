package com.mafour.service.ying;

import lombok.Data;

@Data
public class YingPicture {

  private Long id;

  private String url;

  private String desc;

  private String kind;
}
