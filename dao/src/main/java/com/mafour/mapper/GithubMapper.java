package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.GithubDO;
import org.springframework.stereotype.Component;

@Component
public interface GithubMapper extends BaseMapper<GithubDO> {}
