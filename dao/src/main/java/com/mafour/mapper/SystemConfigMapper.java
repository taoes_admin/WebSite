package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.SystemConfigDO;
import org.springframework.stereotype.Component;

@Component
public interface SystemConfigMapper extends BaseMapper<SystemConfigDO> {}
