package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.book.BookCategoryDO;
import com.mafour.dao.book.BookDO;
import org.springframework.stereotype.Component;

@Component
public interface BookCategoryMapper extends BaseMapper<BookCategoryDO> {}
