package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.CommentDO;
import org.springframework.stereotype.Component;

@Component
public interface CommentMapper extends BaseMapper<CommentDO> {}
