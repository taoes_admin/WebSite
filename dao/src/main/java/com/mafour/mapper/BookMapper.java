package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.book.BookDO;
import org.springframework.stereotype.Component;

@Component
public interface BookMapper extends BaseMapper<BookDO> {}
