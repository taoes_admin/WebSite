package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.YingPictureDO;
import org.springframework.stereotype.Component;

@Component
public interface YingPictureMapper extends BaseMapper<YingPictureDO> {}
