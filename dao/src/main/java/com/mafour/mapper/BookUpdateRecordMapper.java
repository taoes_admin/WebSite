package com.mafour.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mafour.dao.BookUpdateRecordDO;
import org.springframework.stereotype.Component;

@Component
public interface BookUpdateRecordMapper extends BaseMapper<BookUpdateRecordDO> {}
